astropy>=6.0.0
numpy>=1.24.0
packaging>=23.0
parfive[ftp]>=2.0.0
pyerfa>=2.0.1.1
requests>=2.28.0
fsspec>=2023.3.0

[all]
sunpy[asdf,core,jpeg2000,opencv,scikit-image,spice]

[asdf]
asdf-astropy>=0.5.0
asdf>=2.13.0

[core]
sunpy[image,map,net,timeseries,visualization]

[dask]
dask[array]>=2022.5.2

[dev]
sunpy[docs,tests]

[docs]
sphinx>=6.0.0
sphinx-automodapi>=0.14.1
packaging>=23.0
sunpy[all]
hvpy>=1.1.0
ruamel.yaml>=0.17.19
sphinx-changelog>=1.5.0
sphinx-copybutton>=0.5.0
sphinx-design>=0.2.0
sphinx-gallery>=0.13.0
sphinxext-opengraph>=0.6.0
sunpy-sphinx-theme>=2.0.3
sphinx-hoverxref>=1.3.0

[docs-gallery]
sunpy[docs]
astroquery>=0.4.6
jplephem>=2.14

[image]
scipy>=1.10.1

[jpeg2000]
glymur>=0.11.0
lxml!=5.0.0,>=4.9.1

[map]
contourpy>=1.0.1
matplotlib>=3.6.0
mpl-animators>=1.0.0
reproject>=0.10.0
scipy>=1.10.1

[net]
beautifulsoup4>=4.11.0
drms>=0.7.1
python-dateutil>=2.8.1
tqdm>=4.64.0
zeep>=4.1.0

[opencv]
opencv-python>=4.6.0.66

[s3]
fsspec[s3]>=2023.3.0
aiobotocore[boto3]>=1.26.41

[scikit-image]
scikit-image>=0.20.0

[spice]
spiceypy>=5.0.0

[tests]
sunpy[all,s3,tests-only]

[tests-only]
hvpy>=1.1.0
jplephem>=2.14
pytest-astropy>=0.11.0
pytest-mpl>=0.16
pytest-xdist>=3.0.2
pytest>=7.1.0

[timeseries]
cdflib>=1.3.2
h5netcdf>=1.0.0
h5py>=3.8.0
matplotlib>=3.6.0
pandas>=1.5.0

[visualization]
matplotlib>=3.6.0
mpl-animators>=1.0.0
